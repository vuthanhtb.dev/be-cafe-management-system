package com.dev.cafe.controller;

import com.dev.cafe.dto.ResponseDTO;
import com.dev.cafe.dto.request.LoginRequest;
import com.dev.cafe.dto.request.RegisterRequest;
import com.dev.cafe.service.AuthService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;

    @PostMapping("/register")
    public ResponseEntity<ResponseDTO> register(@RequestBody @Valid RegisterRequest registerRequest) {
        return this.authService.register(registerRequest);
    }

    @PostMapping("/login")
    public ResponseEntity<ResponseDTO> login(@RequestBody @Valid LoginRequest loginRequest) {
        return this.authService.login(loginRequest);
    }
}
