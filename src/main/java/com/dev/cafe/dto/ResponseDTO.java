package com.dev.cafe.dto;

import com.dev.cafe.constant.MessageConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseDTO {
    private Object data = null;
    private String status = MessageConstants.STATUS_200;
    private String messageContent = "";
}
