package com.dev.cafe.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "build")
public class RegisterResponse {
    private Integer id;
    private String name;
    private String email;
    private String contractNumber;
    private String status;
    private String role;
    private Instant createdAt;
}
