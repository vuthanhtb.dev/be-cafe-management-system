package com.dev.cafe.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(staticName = "build")
public class LoginResponse {
    private String token;
    private String refreshToken;
    private Instant expiresAt;
    private String email;
}
