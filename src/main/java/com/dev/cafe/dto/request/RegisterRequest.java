package com.dev.cafe.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "build")
public class RegisterRequest {
    @NotNull(message = "name should not be null")
    @NotBlank(message = "name should not be blank")
    private String name;

    @NotNull(message = "email should not be null")
    @NotBlank(message = "email should not be blank")
    private String email;

    @NotNull(message = "password should not be null")
    @NotBlank(message = "password should not be blank")
    private String password;

    @NotNull(message = "contractNumber should not be null")
    @NotBlank(message = "contractNumber should not be blank")
    private String contractNumber;
}
