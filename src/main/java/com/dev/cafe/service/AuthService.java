package com.dev.cafe.service;

import com.dev.cafe.config.HttpResponseConfig;
import com.dev.cafe.constant.ErrorConstants;
import com.dev.cafe.constant.MessageConstants;
import com.dev.cafe.dto.ResponseDTO;
import com.dev.cafe.dto.request.LoginRequest;
import com.dev.cafe.dto.request.RegisterRequest;
import com.dev.cafe.dto.response.LoginResponse;
import com.dev.cafe.entity.UserEntity;
import com.dev.cafe.mapper.AuthMapper;
import com.dev.cafe.repository.UserRepository;
import com.dev.cafe.security.JwtService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class AuthService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthService.class);

    private final UserRepository userRepository;
    private final AuthMapper authMapper;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public ResponseEntity<ResponseDTO> register(RegisterRequest registerRequest) {
        try {
            UserEntity userEntityFound = this.userRepository.findByEmail(registerRequest.getEmail()).orElse(null);
            ResponseDTO response = new ResponseDTO();

            if (Objects.isNull(userEntityFound)) {
                registerRequest.setPassword(this.passwordEncoder.encode(registerRequest.getPassword()));
                UserEntity userEntitySaved = this.userRepository.save(this.authMapper.toUserEntity(registerRequest));
                response.setData(this.authMapper.toRegisterResponse(userEntitySaved));
                response.setStatus(MessageConstants.STATUS_201);
                response.setMessageContent("Successfully registered");
                return HttpResponseConfig.responseHandler(response, HttpStatus.CREATED);
            }

            response.setMessageContent("Email already exist");
            response.setStatus(ErrorConstants.ERR_EMAIL_EXISTED);
            return HttpResponseConfig.responseHandler(response, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            return HttpResponseConfig.errorHandler(String.format("Auth -> Login -> %s", e.getMessage()));
        }
    }

    public ResponseEntity<ResponseDTO> login(LoginRequest loginRequest) {
        try {
            ResponseDTO response = new ResponseDTO();
            Authentication authentication = this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    loginRequest.getEmail(),
                    loginRequest.getPassword()
            ));

            UserEntity principal = (UserEntity) authentication.getPrincipal();
            String token = this.jwtService.generateToken(principal);
            LoginResponse loginResponse = LoginResponse.builder()
                    .token(token)
                    .email(principal.getEmail())
                    .expiresAt(Instant.now().plusMillis(this.jwtService.getJwtExpirationInMillis()))
                    .build();

            response.setData(loginResponse);
            response.setMessageContent("Login successfully");

            return HttpResponseConfig.responseHandler(response, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return HttpResponseConfig.errorHandler(e.getMessage(), HttpStatus.BAD_REQUEST, ErrorConstants.ERR_EMAIL_OR_PASSWORD_INVALID);
        }
    }
}
