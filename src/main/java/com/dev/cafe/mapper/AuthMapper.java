package com.dev.cafe.mapper;

import com.dev.cafe.dto.request.RegisterRequest;
import com.dev.cafe.dto.response.LoginResponse;
import com.dev.cafe.dto.response.RegisterResponse;
import com.dev.cafe.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.Instant;

@Mapper(componentModel = "spring")
public abstract class AuthMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "status", expression = "java(getStatusDefault())")
    @Mapping(target = "role", expression = "java(getRoleDefault())")
    @Mapping(target = "createdAt", expression = "java(getDateNow())")
    @Mapping(target = "updatedAt", expression = "java(getDateNow())")
    public abstract UserEntity toUserEntity(RegisterRequest registerRequest);

    String getRoleDefault() {
        return "USER";
    }

    String getStatusDefault() {
        return "WAITING_FOR_APPROVAL";
    }

    Instant getDateNow() {
        return Instant.now();
    }

    public abstract RegisterResponse toRegisterResponse(UserEntity userEntity);

    public abstract LoginResponse toLoginResponse(UserEntity userEntity);
}
