package com.dev.cafe.constant;

public class ErrorConstants {
    public static final String ERR_SYSTEM_BUSY = "ERR_000";
    public static final String ERR_EMAIL_EXISTED = "ERR_001";
    public static final String ERR_EMAIL_OR_PASSWORD_INVALID = "ERR_003";
    public static String METHOD_ARGUMENT_NOT_VALID = "ERR_002";
}
