package com.dev.cafe.constant;

public class MessageConstants {
    // 599 Connect timeout error
    public static final String STATUS_599 = "599";

    // 598 Read timeout error
    public static final String STATUS_598 = "598";

    // 597 Connect OK, but business error occurred (mix)
    public static final String STATUS_597 = "597";

    // 500 Internal Server Error
    public static final String STATUS_500 = "500";

    // 401 Unauthorized
    public static final String STATUS_401 = "401";

    // 300 Warning (mix)
    public static final String STATUS_300 = "300";

    // 200 OK
    public static final String STATUS_200 = "200";
    public static final String STATUS_201 = "201";

    public static final String STATUS_204 = "204";

    public static final String STATUS_404 = "404";

    public static final String UNKNOWN_CODE = "UNKNOWN_CODE";

    public static final String MESS_SYSTEM_BUSY = "The system is busy. Please come back later.";
}
