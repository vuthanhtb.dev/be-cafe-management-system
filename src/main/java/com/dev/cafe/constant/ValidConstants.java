package com.dev.cafe.constant;

import java.util.regex.Pattern;

public class ValidConstants {
    // mail pattern
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    // mail pattern
    public static final Pattern VALID_NAME_REGEX = Pattern.compile("^[^0-9!<>,;?=+()@#\"°{}_$%:]*$", Pattern.CASE_INSENSITIVE);
}
