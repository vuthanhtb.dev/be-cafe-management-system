package com.dev.cafe.constant;

public class CafeStatus {
    public static String CAFE_200 = "CAFE_200";
    public static String CAFE_201 = "CAFE_201";
    public static String ERROR_400 = "ERROR_400";
    public static String ERROR_500 = "ERROR_500";
    public static String METHOD_ARGUMENT_NOT_VALID = "METHOD_ARGUMENT_NOT_VALID";
    public static String EMAIL_ALREADY_EXIST = "EMAIL_ALREADY_EXIST";
}
