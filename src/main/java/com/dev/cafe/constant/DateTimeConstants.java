package com.dev.cafe.constant;

public class DateTimeConstants {
    public static final String DEFAULT_HHMMSS = " 00:00:00";

    public static final String JSON_DATE_FORMAT = "yyyyMMdd HH:mm:ss";

    public static final String DSR_FORMAT_DATE = "yyyyMMdd";

    public static final String NS_FORMAT_DATE = "yyyy-MM-dd";

    public static final String DSR_FORMAT_TIME = "HHmmss";

    public static final String DEFAULT_DATE_PATTERN = "dd/MM/yyyy";

    public static final String DEFAULT_REPORT_DATE_PATTERN = "dd/MM/yyyy";

    public static final String DEFAULT_TIME_PATTERN = "HH:mm:ss";

    public static final String DEFAULT_DATE_AND_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

    public static final String DEFAULT_SQL_DATE_AND_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static final String EXPORT_DATE_PATTERN = "yyyyMMddHHmmss";

    public static final String SYNC_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static final String CURRENT_DATE_PARAM = "currentDate";

    public static final String DEFAULT_DAILY_REVENUE_REPORT_MONTH_AND_YEAR_PATTERN = "MM.yyyy";

    public static final String DEFAULT_SHEET_NAME_MONTHLY_REPORT_PATTERN = "MM.yyyy";

    public static final String DEFAULT_SHEET_NAME_CLOSING_SUMMARY_REPORT_PATTERN = "ddMMyyyy";

    public static final String DEFAULT_IMPORT_ASN_DATE = "yyyy/MM/dd";

    public static final String DEFAULT_IMPORT_DATE = "yyyy/MM/dd";

    public static final String DEFAULT_CUSTOMER_SELL_OUT_REPORT = "yyyyMMdd";

    public static final String MAIL_DATE_AND_TIME_PATTERN = "dd/MM/yyyy HH:mm";
}
