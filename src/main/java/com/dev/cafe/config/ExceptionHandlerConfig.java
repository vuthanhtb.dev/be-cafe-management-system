package com.dev.cafe.config;

import com.dev.cafe.constant.ErrorConstants;
import com.dev.cafe.dto.ResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ExceptionHandlerConfig {
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ResponseDTO> handleException(MethodArgumentNotValidException exception) {
        Map<String, String> errors = new HashMap<String, String>();
        exception.getBindingResult().getFieldErrors().forEach(error -> {
            errors.put(error.getField(), error.getDefaultMessage());
        });

        ResponseDTO response = new ResponseDTO();
        response.setData(errors);
        response.setMessageContent("argument not valid");
        response.setStatus(ErrorConstants.METHOD_ARGUMENT_NOT_VALID);

        return HttpResponseConfig.responseHandler(response, HttpStatus.BAD_REQUEST);
    }
}
