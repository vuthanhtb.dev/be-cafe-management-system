package com.dev.cafe.config;

import com.dev.cafe.constant.ErrorConstants;
import com.dev.cafe.constant.MessageConstants;
import com.dev.cafe.dto.ResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class HttpResponseConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpResponseConfig.class);

    public static ResponseEntity<ResponseDTO> responseHandler(ResponseDTO responseDTO, HttpStatus httpStatus) {
        return ResponseEntity.status(httpStatus).body(responseDTO);
    }

    public static ResponseEntity<ResponseDTO> errorHandler(String message) {
        LOGGER.error(message);
        ResponseDTO response = new ResponseDTO();
        response.setMessageContent(MessageConstants.MESS_SYSTEM_BUSY);
        response.setStatus(ErrorConstants.ERR_SYSTEM_BUSY);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
    }

    public static ResponseEntity<ResponseDTO> errorHandler(String message, HttpStatus httpStatus, String status) {
        LOGGER.error(message);
        ResponseDTO response = new ResponseDTO();
        response.setMessageContent(message);
        response.setStatus(status);
        return ResponseEntity.status(httpStatus).body(response);
    }
}
